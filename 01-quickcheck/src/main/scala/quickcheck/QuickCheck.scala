package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("min of two elements") = forAll { (elem1: Int, elem2: Int) =>
    val heap1 = insert(elem1, empty)
    val heap2 = insert(elem2, heap1)
    findMin(heap2) == Math.min(elem1, elem2)
  }
  
  property("delete from singe element heap") = forAll { elem: Int =>
    val heap = insert(elem, empty)
    val heapAfterDeleteion = deleteMin(heap)
    isEmpty(heapAfterDeleteion)
  }
  
  property("sorted sequence") = forAll { heap: H =>
    val list = toList(heap)
    list.sorted == list
  }
  
  property("sorted sequence of melding of two heaps") = forAll { (heap1: H, heap2: H) =>
    val meldedHeap = meld(heap1, heap2)
    val meldedList = toList(meldedHeap)
    val list1 = toList(heap1)
    val list2 = toList(heap2)
    meldedList == (list1 ++ list2).sorted
  }
  
  property("min of melding of two heaps") = forAll { (heap1: H, heap2: H) =>
    val meldedHeap = meld(heap1, heap2)
    val minElem = findMin(meldedHeap)
    minElem == findMin(heap1) || minElem == findMin(heap2)
  }
  
  property("melding of a heap and an empty heap") = forAll { heap: H =>
    val meldedHeap = meld(heap, empty)
    meldedHeap == heap
  }
  
  def toList(heap: H): List[Int] =
    if (isEmpty(heap)) Nil 
    else findMin(heap) :: toList(deleteMin(heap))

  lazy val genHeap: Gen[H] = for {
    v <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(v, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
