/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /**
   * Request with identifier `id` to insert an element `elem` into the tree.
   * The actor at reference `requester` should be notified when this operation
   * is completed.
   */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /**
   * Request with identifier `id` to check whether an element `elem` is present
   * in the tree. The actor at reference `requester` should be notified when
   * this operation is completed.
   */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /**
   * Request with identifier `id` to remove the element `elem` from the tree.
   * The actor at reference `requester` should be notified when this operation
   * is completed.
   */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /**
   *  Request to perform garbage collection
   */
  case object GC

  /**
   * Holds the answer to the Contains request with identifier `id`.
   * `result` is true if and only if the element is present in the tree.
   */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply

  /**
   *  Message to signal successful completion of an insert or remove operation.
   */
  case class OperationFinished(id: Int) extends OperationReply

}

class BinaryTreeSet extends Actor with ActorLogging {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /**
   *  Accepts `Operation` and `GC` messages.
   */
  val normal: Receive = {
    case op: Operation => root ! op

    case GC =>
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))

    case _ => log.debug("illegal operation")
  }

  // optional
  /**
   * Handles messages while garbage collection is performed.
   * `newRoot` is the root of the new binary tree where we want to copy
   * all non-removed elements into.
   */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case CopyFinished => {
      val oldRoot = root
      root = newRoot
      pendingQueue foreach { operation => root ! operation }
      pendingQueue = Queue.empty[Operation]
      oldRoot ! PoisonPill
      context.become(normal)
    }
    case op: Operation => pendingQueue = pendingQueue.enqueue(op)
    case GC            => log.debug("ignore GC request")
    case _             => log.debug("illegal operation")
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode], elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor with ActorLogging {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  def createNode(elem: Int): ActorRef =
    context.actorOf(BinaryTreeNode.props(elem, initiallyRemoved = false), "node-" + elem)

  // optional
  def receive = normal

  // optional
  /**
   *  Handles `Operation` messages and `CopyTo` requests.
   */
  val normal: Receive = insert orElse contains orElse remove orElse copyTo

  /**
   *  Handles the Insert `Operation` message.
   */
  def insert: Receive = {
    case op @ Insert(requester, id, thatElem) if (thatElem < elem) =>
      insert(Left, op)

    case op @ Insert(requester, id, thatElem) if (thatElem > elem) =>
      insert(Right, op)

    case Insert(requester, id, thatElem) if (thatElem == elem) =>
      removed = false
      requester ! OperationFinished(id)
  }

  /**
   *  Handles the Contains `Operation` message.
   */
  def contains: Receive = {
    case op @ Contains(requester, id, thatElem) if (thatElem < elem) =>
      contains(Left, op)

    case op @ Contains(requester, id, thatElem) if (thatElem > elem) =>
      contains(Right, op)

    case Contains(requester, id, thatElem) if (thatElem == elem) =>
      requester ! ContainsResult(id, !removed)
  }

  /**
   *  Handles the Remove `Operation` message.
   */
  def remove: Receive = {
    case op @ Remove(requester, id, thatElem) if (thatElem < elem) =>
      remove(Left, op)

    case op @ Remove(requester, id, thatElem) if (thatElem > elem) =>
      remove(Right, op)

    case op @ Remove(requester, id, thatElem) if (thatElem == elem) =>
      removed = true
      requester ! OperationFinished(id)
  }

  /**
   *  Handles the CopyTo message.
   */
  def copyTo: Receive = {
    case CopyTo(treeNode: ActorRef) =>
      log.debug("copyTo {} element {}", treeNode, elem)

      val expected = subtrees.values.toSet

      if (expected.isEmpty & removed) context.parent ! CopyFinished
      else context.become(copying(expected, removed))

      if (!removed) treeNode ! Insert(self, elem, elem)

      expected foreach {
        actorRef => actorRef ! CopyTo(treeNode)
      }
  }

  // optional
  /**
   * `expected` is the set of ActorRefs whose replies we are waiting for,
   * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
   */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case CopyFinished         => checkCopyDone(expected - sender, insertConfirmed)
    case OperationFinished(_) => checkCopyDone(expected, true)
    case _                    => log.debug("illegal operation")
  }

  /**
   * Corresponding helper.
   */
  def checkCopyDone(expected: Set[ActorRef], insertConfirmed: Boolean) = {
    if (expected.isEmpty && insertConfirmed) {
      context.parent ! CopyFinished
      context.become(normal)
    } else {
      context.become(copying(expected, insertConfirmed))
    }
  }

  /**
   * Corresponding helper.
   */
  def insert(pos: Position, op: Operation) = {
    if (subtrees contains pos) {
      subtrees(pos) ! op
    } else {
      val newNode = createNode(op.elem)
      subtrees = subtrees.updated(pos, newNode)
      op.requester ! OperationFinished(op.id)
    }
  }

  /**
   * Corresponding helper.
   */
  def contains(pos: Position, op: Operation) = subtrees.get(pos) match {
    case Some(_) => subtrees(pos) ! op
    case _       => op.requester ! ContainsResult(op.id, false)
  }

  /**
   * Corresponding helper.
   */
  def remove(pos: Position, op: Operation) = subtrees.get(pos) match {
    case Some(_) => subtrees(pos) ! op
    case _       => op.requester ! OperationFinished(op.id)
  }

}
